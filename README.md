## CPU Load Data Pipeline
### Simple Kafka producer and consumer

##### Important Note:
_**This isn't meant to be a complete production App as Aiven's team would hope for.
Unfortunately, I only had about 1.5 hour of time to discover Aiven's services console, 
create a Kafka cluster, and a simple Java Maven project with 2 modules that connect to Aiven's
Kafka, produces and consumes data. 
No validation, no special data objects and serialization/deserialization, partitioning, offset, 
or full integration**_

##### Overview

- A simple Java 8 maven project with two modules.
- Producer module collects information about CPU load and pushes to a Kafka topic.
- Consumer module consumes data about CPU load from Kafka topic.
- Saving data into Postgresql is not implemented. Please check comments in the code.
- There isn't many features to be tested here. Only basic test is provided for some classes.
- Ideally, I would write an integration test with embedded service or a proper simulator of a broker.
- I tested this producer-consumer solution against free credit Aiven's Kafka I have created via Aiven Console.

#### Build and Run

App configuration:
```
-Dtopic="xxx"
-DkafkaHost="xxx.aivencloud.com:20797"
-DtruststoreAbsolutePath="/home/xxx/client.truststore.jks"
-DtruststorePassword="xxx"
-DkeystoreAbsolutePath="/home/xxx/client.keystore.p12"
-DkeystorePassword="xxx"
-DsslKeyPassword="xxx"
```

In the parent directory:
```
mvn clean package assembly:single
```

Run producer:
```
java -jar -Dtopic="xxx" -DkafkaHost="xxx.aivencloud.com:20797" -DtruststoreAbsolutePath="/home/xxx/client.truststore.jks" -DtruststorePassword="xxx" -DkeystoreAbsolutePath="/home/xxx/client.keystore.p12" -DkeystorePassword="xxx" -DsslKeyPassword="xxx" data-producer/target/data-producer-1.0-jar-with-dependencies.jar
```

Run consumer:
```
java -jar -Dtopic="xxx" -DkafkaHost="xxx.aivencloud.com:20797" -DtruststoreAbsolutePath="/home/xxx/client.truststore.jks" -DtruststorePassword="xxx" -DkeystoreAbsolutePath="/home/xxx/client.keystore.p12" -DkeystorePassword="xxx" -DsslKeyPassword="xxx" data-consumer/target/data-consumer-1.0-jar-with-dependencies.jar
```

I wished I had time to do more, being totally packed up and strangled in a critical work and personal schedule this month,
I thought sending something is still better than nothing.
I'm happy to discuss this solution and provide more ideas to evolve it later.